(ns dev.ruivieira.valis.log
  (:require [clj-commons.ansi :as ansi]))

(defn ^String ok [^String x]
    (println (str "🤖, ok: " (ansi/compose [:green x]))))