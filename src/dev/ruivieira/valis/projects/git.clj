(ns dev.ruivieira.valis.projects.git
  (:require [clojure.string :as str]
            [babashka.process :as p]))

(defn clone-branch-repo-shallow
  "Shallow clone of a specific git branch into a given directory."
  [^String url ^String branch ^String dir]
  (assert (instance? String dir) "dir must be an instance of String")
  (p/sh (str/join " " ["git" "clone" "--branch" branch "--depth" "1" url dir])))
