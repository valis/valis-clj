(ns dev.ruivieira.valis.core
  (:require [clojure.java.io :as io]
            [clojure.string :as string])
  (:import java.nio.file.Files
           java.nio.file.Path
           java.nio.file.attribute.FileAttribute))

(defn date-difference-in-days
  "Takes two dates in 'yyyy-MM-dd' format and returns the difference in days."
  [^String date1 ^String date2]
  (let [fmt (java.time.format.DateTimeFormatter/ofPattern "yyyy-MM-dd")
        date1 (java.time.LocalDate/parse date1 fmt)
        date2 (java.time.LocalDate/parse date2 fmt)]
    (-> date1
        (.until date2 java.time.temporal.ChronoUnit/DAYS))))

(defn expand-file-name
  "Function providing functionality similar to Emacs' `expand-file-name`."
  [^String file-name]
  (let [user-home (System/getProperty "user.home")
        expanded (string/replace file-name #"^~" user-home)]
    (-> expanded io/file .getAbsolutePath)))


