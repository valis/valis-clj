(ns dev.ruivieira.valis.k8s.k8s
  (:require [babashka.process :as p]
            [cheshire.core :as json]
            [clj-yaml.core :as yaml]))

(defn resource-apply-str
  ([resource-map] (resource-apply-str resource-map nil))
  ([resource-map namespace]
   (let [json-str (json/generate-string resource-map)
         yaml-str (yaml/generate-string (json/parse-string json-str) {:dumper-options {:flow-style :block}})
         command (if namespace (str "kubectl apply -n " namespace " -f -")
                             "kubectl apply -f -")]
     {:command command :input yaml-str})))

(defn resource-apply
  ([resource-map] (resource-apply resource-map nil))
  ([resource-map namespace]
   (let [{:keys [command input]} (resource-apply-str resource-map namespace)
         {:keys [exit out err]} (p/shell command {:in input})]
     (if (zero? exit)
       (println "Successfully applied:\n" out)
       (do
         (println "Error applying:\n" err)
         (throw (Exception. "Error applying kubectl command")))))))

;; Use it like this:
;; Without namespace:
;; (apply {:kind "Pod" :metadata {:name "my-pod"} :spec {}})

;; With namespace:
;; (apply {:kind "Pod" :metadata {:name "my-pod"} :spec {}} "my-namespace")