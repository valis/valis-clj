(ns dev.ruivieira.valis.k8s.kind
  (:require [babashka.process :as p]))

(defn create-cluster
  "Create a KinD cluster using the provided arguments"
  [opts]
  (let [command "kind create cluster"
        image-arg (when-let [image (:image opts)]
                    (str " --image=kindest/node:" image))
        config-arg (when-let [config (:config opts)]
                     (str " --config " config))
        name-arg (when-let [name (:name opts)]
                   (str " --name " name))
        retain-arg (when (:retain opts)
                     " --retain")
        wait-arg (when-let [wait (:wait opts)]
                   (str " --wait " wait))
        kubeconfig-arg (when-let [kubeconfig (:kubeconfig opts)]
                         (str " --kubeconfig " kubeconfig))
        cmd (str command image-arg config-arg name-arg retain-arg wait-arg kubeconfig-arg)]
    (p/shell {:inherit true} cmd)))

(defn delete-cluster
  "Delete a KinD cluster using the provided arguments"
  [opts]
  (let [command "kind delete cluster"
        name-arg (when-let [name (:name opts)]
                   (str " --name " name))
        kubeconfig-arg (when-let [kubeconfig (:kubeconfig opts)]
                         (str " --kubeconfig " kubeconfig))
        cmd (str command name-arg kubeconfig-arg)]
    (p/shell {:inherit true} cmd)))